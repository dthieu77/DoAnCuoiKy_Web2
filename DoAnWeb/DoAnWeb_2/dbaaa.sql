
/****** Object:  Database [DoAnWebShopOnline]    Script Date: 19/06/2017 12:29:46 SA ******/
CREATE DATABASE [DoAnWebShopOnline]
  
GO
USE [DoAnWebShopOnline]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BinhLuan]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BinhLuan](
	[MaBinhLuan] [int] IDENTITY(1,1) NOT NULL,
	[MaSanPham] [int] NOT NULL,
	[MaTaiKhoan] [nvarchar](128) NOT NULL,
	[TenTaiKhoan] [nvarchar](128) NOT NULL,
	[NoiDung] [nvarchar](2048) NOT NULL,
	[TinhTrang] [int] NOT NULL CONSTRAINT [DF_BinhLuan_TinhTrang]  DEFAULT ((1)),
	[Ngay] [datetime] NOT NULL CONSTRAINT [DF_BinhLuan_Ngay]  DEFAULT (getdate()),
 CONSTRAINT [PK_BinhLuan] PRIMARY KEY CLUSTERED 
(
	[MaBinhLuan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GioHang]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GioHang](
	[Id] [int] NOT NULL,
	[MaTaiKhoan] [nvarchar](128) NOT NULL,
	[MaSanPham] [int] NULL,
	[TenSP] [nvarchar](50) NULL,
	[Gia] [int] NULL,
	[SoLuong] [int] NULL,
	[TongTien] [int] NULL,
 CONSTRAINT [PK_GioHang] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LienHe]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LienHe](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TenKhachHang] [nvarchar](200) NULL,
	[DiaChi] [nvarchar](200) NULL,
	[SDT] [nchar](12) NULL,
	[NoiDung] [nvarchar](200) NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_LienHe] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiSanPham]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiSanPham](
	[MaLoaiSanPham] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiSanPham] [nvarchar](100) NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_LoaiSanPham] PRIMARY KEY CLUSTERED 
(
	[MaLoaiSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhaSanXuat]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhaSanXuat](
	[MaNhaSanXuat] [int] IDENTITY(1,1) NOT NULL,
	[TenNhaSanXuat] [nvarchar](200) NULL,
	[TinhTrang] [int] NULL,
 CONSTRAINT [PK_NhaSanXuat] PRIMARY KEY CLUSTERED 
(
	[MaNhaSanXuat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SanPham]    Script Date: 19/06/2017 12:29:46 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SanPham](
	[MaSanPham] [int] IDENTITY(1,1) NOT NULL,
	[MaLoaiSanPham] [int] NULL,
	[MaNhaSanXuat] [int] NULL,
	[TenSanPham] [nvarchar](200) NULL,
	[ManHinh] [nvarchar](200) NULL,
	[HeDieuHanh] [nvarchar](200) NULL,
	[CaneraTruoc] [nvarchar](200) NULL,
	[CameraSau] [nvarchar](200) NULL,
	[Gia] [int] NULL,
	[CPU] [nvarchar](200) NULL,
	[Ram] [nvarchar](200) NULL,
	[BoNhoTrong] [nvarchar](200) NULL,
	[DungLuongPin] [nvarchar](200) NULL,
	[TinhTrang] [int] NULL,
	[MoTa] [nvarchar](1000) NULL,
	[HinhAnh] [nvarchar](200) NULL,
	[Home] [int] NULL,
 CONSTRAINT [PK_SanPham] PRIMARY KEY CLUSTERED 
(
	[MaSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201706170815153_InitialCreate', N'DoAnWeb_2.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5CDB6EE336107D2FD07F10F4D416A99538DDC536B05BA44ED206DD5CB0CEB67D0B68897684952855A2D20445BFAC0FFDA4FE42871275E345175BB19D628145440ECF0C874372381CFADFBFFF997CFFE47BC6238E62372053F36874681A98D881E392D5D44CE8F2EB77E6F7DF7DFED9E4DCF19F8C5F72BA6346072D493C351F280D4F2C2BB61FB08FE291EFDA5110074B3AB203DF424E608D0F0FBFB58E8E2C0C10266019C6E44342A8EBE3F4033E6701B1714813E45D050EF6625E0E35F314D5B8463E8E4364E3A979169C925FF1E27E3CCA684DE3D47311C831C7DED2341021014514A43CF918E3398D02B29A875080BCBBE71003DD127931E6D29F94E45D3B7238661DB1CA8639949DC434F07B021E1D73CD5862F3B5F46B169A03DD9D838EE933EB75AABFA979E9E0B4E843E08102448627332F62C453F3AA60711A87D7988EF286A30CF22202B83F82E8D3A88A7860746E775058D27874C8FE1D18B3C4A34984A704273442DE81719B2C3CD7FE193FDF059F30991E1F2D96C7EFDEBC45CEF1DB6FF0F19B6A4FA1AF40572B80A2DB28087104B2E165D17FD3B0EAED2CB161D1ACD226D30AD8124C0AD3B8424FEF3159D107982EE377A671E13E61272FE1C6F591B83087A0118D12F8BC4E3C0F2D3C5CD45B8D3CD9FF0D5CC76FDE0EC2F51A3DBAAB74E805FE30712298571FB097D6C60F6E984DAFDA78DF73B28B28F0D977DDBEB2DAFB79904436EB4CA025B943D10AD3BA7413AB34DE4E26CDA08637EB1C75FF4D9B492A9BB7929475689D9990B3D8F66CC8E57D59BE9D2DEE340C61F052D3621A69323871AB1A096D0F8C82A2349BA3AE6643A03BFFE755F0DC47AE37C032D8810BF81F4B37F271D1CB1F02303A447ACB7C8BE2185601E727143F34880E7F0E20FA1CDB4904C639A7C80F5F9CDBED4340F075E22F98CD6F8FD7604373F74770816C1A44E784B5DA18EF7D607F0A127A4E9C3344F1476AE780ECF3CEF5BB030C22CEA96DE338BE0063C6CE2C00F73A07BC24F478DC1B8E2D4FBB7643661E727DB51F222CA4F73969E98BA829247F4443A6F2499A447D1FAC5CD24DD49C542F6A46D12A2A27EB2B2A03EB2629A7D40B9A12B4CA99510DE6E5A52334BC9B97C2EEBF9FB7D9E6AD5B0B2A6A9CC30A897FC40447B08C39B788521C917204BAAC1BBB7016D2E1634C5F7C6F4A39FD82BC6468566BCD867411187E36A4B0FB3F1B5231A1F8D1759857D2E1F09313037C277AF5B9AA7DCE09926D7B3AD4BAB96DE6DB590374D3E5348E03DB4D678122ECC5831675F9C18733DA2318596FC42808740C0CDD655B1E9440DF4CD1A86EC819F630C5C6A99D85056728B69123AB113AE4F4102CDF51158295D190BA705F493CC1D271C41A2176088A61A6BA84CAD3C225B61B22AF554B42CB8E5B18EB7BC143AC39C321268C61AB26BA3057073F9800051F6150DA3434B12A16D76C881AAF5537E66D2E6C39EE524C622B36D9E23B6BEC92FB6F2F6298CD1ADB827136ABA48B00DA40DE2E0C949F55BA1A807870D93703154E4C1A03E52ED5560CB4AEB11D18685D25AFCE40B3236AD7F117CEABFB669EF583F2F6B7F54675EDC0366BFAD833D3CC7C4F6843A1058E64F33C5BB04AFC4415873390939FCF62EEEA8A26C2C0E798D64336A5BFABF443AD6610D1889A004B436B01E55780129034A17A0897C7F21AA5E35E440FD83CEED608CBD77E01B662033276F52AB442A8BF30158DB3D3E9A3E859610D9291773A2C54701406212E5EF58E77508A2E2E2B2BA68B2FDCC71BAE748C0F4683825A3C578D92F2CE0CAEA5DC34DBB5A472C8FAB8641B6949709F345ACA3B33B896B88DB62B49E114F4700B3652517D0B1F68B2E5918E62B729EA2656961FC50B269626916A7285C2D025AB4A62152F31E65956D5ECEB79FF84233FC3B0EC58917754485B70A241845658A805D620E9851BC5F40C51B4402CCE33737C894CB9B76A96FF9C6575FB940731DF07726AF677D642BAB8AFEDB4B22BC2112EA07F3EF367D220BA62F4D5CD0D96E6863C1429E2F6B3C04B7CA277AFF4ADB3DBBB6AFBAC4446985882FC92FB24E94A7272EB8AEF342CF2941864880ADF65FD61D243E8949D7B9E5575EBBC513D4A1E9CAAA2E802563B1B369D13D363A844EFB0FF48B522BCCC9CE2292955005ED413A392D5208155EABAA3D6134FAA98F59AEE884276491552A8EA21653587A42664B5622D3C8D46D514DD39C859235574B9B63BB2227FA40AADA85E035B21B358D71D5591625205565477C72EF34DC425748F772DED9965CD6D2B3BD46EB66F69305E663D1C66DBABDCDD57812AC53DB1F8EDBC04C6CBF7D296B427BB356D298B646C664B1A0CFDAA53BBF3AE2F3A8D17F57ACCDA45766D616FBAC8D7E3F5B3D817B50BE958279214DC8BE39D708C9BF02355FBA319E98C95919846AE46D8D49F638AFD112318CD7FF7669E8BD9129E135C21E22E714CB3E40D737C7834165EDEECCF2B182B8E1D4F7124D53D85A98FD916F2B0C8238AEC0714C959111BBC142941A580F32571F0D3D4FC336D7592C62ED85F69F18171197F24EEEF0954DC450936FE92B33C87C99C6F3E5EEDE93B87EE5ABDFCED3E6B7A60DC4430634E8C434197EB8C70FDF5432F69B2A61B48B3F69B88D73BA16A8F0E94A8C28458FF8DC1C2A583BC2FC8A5FCC2474F5FF6154DF986602344C53B81A1F00651A1EE1DC03A58DA37000E7CD2F40D40BFCEAADF04AC239AF63D804BFA8389AF01BA2F4379CB1D6E358A13D13696A454CFADD9D41BA556EE7A6F9292AE379AE87262750FB80D92A7D7B08C5796773CD8EEA8482B1E0C7B97A6FDE2B9C4FB923E5C2676EC366B789B89C20D1742FFABFCE03DC8685364E8EC3E0B78DBB6A68BE2EE792A65BF5CDF3D33369EB7B5FB8CDE6D1B9B2ECCBBE7C6D62B6F77CF6C6D57FBE78E2DADF316BAF32C5C39A148731BA38A05B765D966817338E12F023082CCA3CC1E47AAD3BA9A52525B1896247AA6FA7C3291B1347124BE124533DB7E7DE51B7E6367394D335B4D1666136FBEFE37F2E634CDBC35B98DBBC80F566617AA72B65BD6B1A6F4A7D7940F5CEB494BFA799BCFDA78B5FE9AD27F07514A6DF668EE885F4FB6EF202A1972EAF4C8EE95AF7B61EFACFC9222ECDFB1BB2A21D8EF2A126CD776CD82E6922C837CF31624CA498408CD15A6C8812DF534A2EE12D914AA598C397DDD9DC6EDD84DC7023B97E426A16142A1CBD85F78B5801773029AF8A729CC7599273761FA4325437401C474596CFE86FC90B89E53C87DA1880969209877C123BA6C2C298BECAE9E0BA4EB807404E2EA2B9CA23BEC871E80C537648E1EF13AB281F9BDC72B643F9711401D48FB40D4D53E3973D12A427ECC31CAF6F00936ECF84FDFFD076A781B9D50540000, N'6.1.3-40302')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'bb84fa12-d280-414b-bfc4-43972fc06b14', N'Admin')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c0966586-e2de-4cc6-bc81-e659a354a5b4', N'bb84fa12-d280-414b-bfc4-43972fc06b14')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'4a4bb133-a856-4df7-99b2-0da36bd43f35', N'phat@gmail.com', 0, N'ADMQK28/mrN3Yt8n3GXj9XuXJVdp452sHJaHwlBMJmOMIo6x4h7XNk/ljAR0tOOs6w==', N'9db764a4-d099-4d26-b468-78cca2cf9926', NULL, 0, 0, NULL, 1, 0, N'phat@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8adff9df-3721-4b50-9d88-6ecce34ee8f4', N'hphat@gmail.com', 0, N'ANIyma6qgszmwKLP9bAV3/+xzs2ByaWtCuFnrkqNQA5KGoKAgFnoEefSTR0gGo7pSQ==', N'd8dc328b-f388-4b92-a84c-7ddfe70f6c98', N'123456', 0, 0, NULL, 1, 0, N'hphat@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'9cc67e3c-ee2b-4ee8-937e-83f1516c9c47', N'vinh@gmail.com', 0, N'ALe/a9W3jL2iOxXj6nO2nWguWk6T07lQUeOrOdW8eoU0a6iTA1wS4RW4wt2lsEaWuQ==', N'5e7c40ae-3b28-4c99-971a-0b3159802eff', N'01635386653', 0, 0, NULL, 1, 0, N'vinh@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c0966586-e2de-4cc6-bc81-e659a354a5b4', N'trunghieu@gmail.com', 0, N'AL4Nu4MVWgowsyZJcxLnr51UaLsNUsJIC8k3N7qGG6EPA3rLUvwESpox8YSTJ3PxpQ==', N'5adf0d89-e2cf-4f93-9471-9b1b9f309d8b', NULL, 0, 0, NULL, 0, 0, N'trunghieu@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e82f6435-5ae3-4895-82c6-4853f99c7300', N'dthieu271995@gmail.com', 0, N'ANcUdsuzyEXtURyYky82PA4PNI41e5iz31hn6vi8/oSZJMXkI7/UWXPDdI8VlzApvA==', N'604916cf-05a9-498c-a894-467a34c1a6b4', NULL, 0, 0, NULL, 1, 0, N'dthieu271995@gmail.com')
SET IDENTITY_INSERT [dbo].[BinhLuan] ON 

INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSanPham], [MaTaiKhoan], [TenTaiKhoan], [NoiDung], [TinhTrang], [Ngay]) VALUES (1, 15, N'4a4bb133-a856-4df7-99b2-0da36bd43f35', N'phat@gmail.com', N'hggjhgyijh
', 1, CAST(N'2017-06-17 21:52:38.967' AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSanPham], [MaTaiKhoan], [TenTaiKhoan], [NoiDung], [TinhTrang], [Ngay]) VALUES (2, 15, N'4a4bb133-a856-4df7-99b2-0da36bd43f35', N'phat@gmail.com', N'vbvnbvnb', 1, CAST(N'2017-06-17 21:52:51.513' AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSanPham], [MaTaiKhoan], [TenTaiKhoan], [NoiDung], [TinhTrang], [Ngay]) VALUES (3, 15, N'4a4bb133-a856-4df7-99b2-0da36bd43f35', N'phat@gmail.com', N'hgjhjhjhuybvhbjhbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb', 1, CAST(N'2017-06-17 21:53:09.760' AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSanPham], [MaTaiKhoan], [TenTaiKhoan], [NoiDung], [TinhTrang], [Ngay]) VALUES (4, 1, N'4a4bb133-a856-4df7-99b2-0da36bd43f35', N'phat@gmail.com', N'sadasfafafsaf', 1, CAST(N'2017-06-17 22:02:31.427' AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSanPham], [MaTaiKhoan], [TenTaiKhoan], [NoiDung], [TinhTrang], [Ngay]) VALUES (5, 1, N'4a4bb133-a856-4df7-99b2-0da36bd43f35', N'phat@gmail.com', N'hieu hien', 1, CAST(N'2017-06-17 22:18:20.610' AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSanPham], [MaTaiKhoan], [TenTaiKhoan], [NoiDung], [TinhTrang], [Ngay]) VALUES (6, 1, N'4a4bb133-a856-4df7-99b2-0da36bd43f35', N'phat@gmail.com', N'abc', 1, CAST(N'2017-06-17 22:23:28.570' AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSanPham], [MaTaiKhoan], [TenTaiKhoan], [NoiDung], [TinhTrang], [Ngay]) VALUES (7, 1, N'4a4bb133-a856-4df7-99b2-0da36bd43f35', N'phat@gmail.com', N'sdfsd', 1, CAST(N'2017-06-17 22:26:29.220' AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSanPham], [MaTaiKhoan], [TenTaiKhoan], [NoiDung], [TinhTrang], [Ngay]) VALUES (8, 1, N'4a4bb133-a856-4df7-99b2-0da36bd43f35', N'phat@gmail.com', N'Sản phẩm đẹp!', 1, CAST(N'2017-06-17 22:37:23.760' AS DateTime))
INSERT [dbo].[BinhLuan] ([MaBinhLuan], [MaSanPham], [MaTaiKhoan], [TenTaiKhoan], [NoiDung], [TinhTrang], [Ngay]) VALUES (9, 1, N'8adff9df-3721-4b50-9d88-6ecce34ee8f4', N'hphat@gmail.com', N'sản phẩm tốt', 1, CAST(N'2017-06-18 02:51:02.347' AS DateTime))
SET IDENTITY_INSERT [dbo].[BinhLuan] OFF
SET IDENTITY_INSERT [dbo].[LienHe] ON 

INSERT [dbo].[LienHe] ([ID], [TenKhachHang], [DiaChi], [SDT], [NoiDung], [Email]) VALUES (1, N'đà', N'fff', N'ddđ         ', N'fff', N'faffafaasf')
SET IDENTITY_INSERT [dbo].[LienHe] OFF
SET IDENTITY_INSERT [dbo].[LoaiSanPham] ON 

INSERT [dbo].[LoaiSanPham] ([MaLoaiSanPham], [TenLoaiSanPham], [TinhTrang]) VALUES (1, N'Điện Thoại Tầm Trung', 1)
INSERT [dbo].[LoaiSanPham] ([MaLoaiSanPham], [TenLoaiSanPham], [TinhTrang]) VALUES (2, N'Android', 1)
INSERT [dbo].[LoaiSanPham] ([MaLoaiSanPham], [TenLoaiSanPham], [TinhTrang]) VALUES (3, N'IOS', 1)
INSERT [dbo].[LoaiSanPham] ([MaLoaiSanPham], [TenLoaiSanPham], [TinhTrang]) VALUES (4, N'ds', 0)
INSERT [dbo].[LoaiSanPham] ([MaLoaiSanPham], [TenLoaiSanPham], [TinhTrang]) VALUES (5, N'sdfg', 0)
SET IDENTITY_INSERT [dbo].[LoaiSanPham] OFF
SET IDENTITY_INSERT [dbo].[NhaSanXuat] ON 

INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (1, N'IPhone', 1)
INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (2, N'SamSung', 1)
INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (3, N'Sony', 1)
INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (4, N'OPPO', 1)
INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (5, N'HTC', 1)
INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (6, N'NoKia', 1)
INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (7, N'hfhgf', 0)
INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (8, N'b', 0)
INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (9, N'v', 0)
INSERT [dbo].[NhaSanXuat] ([MaNhaSanXuat], [TenNhaSanXuat], [TinhTrang]) VALUES (10, N'dsfs', 0)
SET IDENTITY_INSERT [dbo].[NhaSanXuat] OFF
SET IDENTITY_INSERT [dbo].[SanPham] ON 

INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (1, 3, 1, N'iPhone 7 Plus', N'LED-backlit IPS LCD, 5.5", Retina HD', N'iOS 10', N'7 MP', N'Hai camera 12 MP', 21990000, N'	Apple A10 Fusion 4 nhân 64-bit', N'3 GB', N'32 GB', N'2900 mAh', 1, NULL, N'/assets/images/iphone1.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (2, 3, 1, N'iPhone 6s', N'LED-backlit IPS LCD, 4.7", Retina HD', N'iOS 9', N'5 MP', N'12 MP', 17945500, N'Apple A9 2 nhân 64-bit', N'	2 GB', N'128 GB', N'1715 mAh', 1, NULL, N'/assets/images/iphone2.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (3, 3, 1, N'iPhone 7 Red', N'	LED-backlit IPS LCD, 4.7", Retina HD', N'iOS 10', N'7 MP', N'12 MP', 19990000, N'	Apple A10 Fusion 4 nhân 64-bit', N'	2 GB', N'128 GB', N'1960 mAh', 1, NULL, N'/assets/images/iphone3.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (4, 3, 1, N'iPhone SE', N'IPS LCD, 4", Retina', N'iOS 9', N'1.2 MP', N'12 MP', 7990000, N'Apple A9 2 nhân 64-bit', N'	2 GB', N'	16 GB', N'	1642 mAh', 1, NULL, N'/assets/images/iphone4.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (5, 2, 2, N'Samsung S8', N'	Super AMOLED, 5.8", Quad HD (2K)', N'	Android 7.0', N'		8 MP', N'	12 MP', 6990000, N'	Exynos 8895 8 nhân 64-bit', N'	Exynos 8895 8 nhân 64-bit', N'	64 GB', N'	3000 mAh, có sạc nhanh', 1, NULL, N'/assets/images/samsung1.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (6, 2, 2, N'Samsung S7', N'	Super AMOLED, 5.5", Quad HD (2K)', N'	Android 6.0 (Marshmallow)', N'	5 MP', N'	12 MP', 15490000, N'	Exynos 8890 8 nhân 64-bit', N'	4 GB', N'	32 GB', N'	3600 mAh, có sạc nhanh', 1, NULL, N'/assets/images/samsung2.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (7, 2, 2, N'Samsung A7', N'Super AMOLED, 5.7", Full HD', N'	Android 6.0 (Marshmallow)', N'	16 MP', N'	16 MP', 10990000, N'	Exynos 7880 8 nhân 64-bit', N'	32 GB', N'	3 GB', N'	3600 mAh, có sạc nhanh', 1, NULL, N'/assets/images/samsung3.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (8, 2, 2, N'Samsung A5', N'	Super AMOLED, 5.2", Full HD', N'Android 6.0 (Marshmallow)', N'	16 MP', N'	16 MP', 8990000, N'	Exynos 7880 8 nhân 64-bit', N'	4 GB', N'	32 GB', N'	3600 mAh, có sạc nhanh', 1, NULL, N'/assets/images/samsung4.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (9, 2, 3, N'Sony XA1', N'	IPS LCD, 6", Full HD', N'	Android 7.0', N'	16 MP', N'	23 MP', 8990000, N'	Mediatek Helio P20', N'	4 GB', N'	64 GB', N'2700 mAh', 1, NULL, N'/assets/images/sony1.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (10, 2, 3, N'Sony Xperia X', N'	IPS LCD, 5", Full HD', N'	Android 6.0 (Marshmallow)', N'	13 MP', N'	23 MP', 7990000, N'	Snapdragon 650 6 nhân 64-bit', N'	3 GB', N'	64 GB', N'	2620 mAh', 1, NULL, N'/assets/images/sony2.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (12, 2, 3, N'Sony Xperia M5', N'	IPS LCD, 5", Full HD', N'	Android 5.0 (Lollipop)', N'	13 MP', N'	21.5 MP', 5391000, N'	MT6795 (Helio x10) 8 nhân 64-bit', N'	3 GB', N'	16 GB', N'	2600 mAh', 1, NULL, N'/assets/images/sony3.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (13, 2, 3, N'Sony Xperia XZ', N'	TRILUMINOS™, 5.2", Full HD', N'	Android 7.0', N'	13 MP', N'	23 MP', 10990000, N'Snapdragon 820 4 nhân 64-bit', N'	3 GB', N'	64 GB', N'	2900 mAh, có sạc nhanh', 1, NULL, N'/assets/images/sony4.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (14, 2, 4, N'OPPO F3', N'	IPS LCD, 5.5", Full HD', N'	Android 6.0 (Marshmallow)', N'16 MP và 8 MP', N'13 MP', 6990000, N'	MT6750T 8 nhân 64-bit', N'	4 GB', N'	64 GB', N'	3200 mAh', 1, NULL, N'/assets/images/oppo1.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (15, 2, 4, N'OPPO A39 ', N'	IPS LCD, 5.2", HD', N'	Android 5.1 (Lollipop)', N'	5 MP', N'	13 MP', 4290000, N'	Mediatek MT6750 8 nhân', N'	3 GB', N'	32 GB', N'	2900 mAh', 1, NULL, N'/assets/images/oppo2.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (16, 2, 4, N'OPPO F3 Plus', N'	IPS LCD, 6", Full HD', N'	Android 6.0 (Marshmallow)', N'	16 MP và 8 MP', N'	16 MP', 10690000, N'	Snapdragon 653 8 nhân 64-bit', N'	4 GB', N'	64 GB', N'	4000 mAh, có sạc nhanh', 1, NULL, N'/assets/images/oppo3.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (17, 2, 4, N'OPPO A37', N'	IPS LCD, 5", HD', N'Android 5.1 (Lollipop)', N'	5 MP', N'	8 MP', 3290000, N'	Qualcomm Snapdragon 410 4 nhân 64-bit', N'	2 GB', N'	16 GB', N'	2630 mAh', 1, NULL, N'/assets/images/oppo4.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (18, 1, 5, N'HTC U Play', N'	Super LCD, 5.2", Full HD', N'	Android 6.0 (Marshmallow)', N'	16 MP', N'	16 MP', 8990000, N'	MTK Helio P10 8 nhân 64-bit', N'	3 GB', N'	32 GB', N'	2500 mAh, có sạc nhanh', 1, NULL, N'/assets/images/htc1.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (19, 1, 5, N'HTC U Ultra', N'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)', N'	Android 7.0', N'	16 MP', N'	12 MP', 16990000, N'	Qualcomm Snapdragon 821 4 nhân 64-bit', N'	4 GB', N'	128 GB', N'	3000 mAh, có sạc nhanh', 1, NULL, N'/assets/images/htc2.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (20, 1, 5, N'HTC One A9', N'	Super LCD, 5", HD', N'Android 6.0 (Marshmallow)', N'	5 MP', N'	13 MP', 5190000, N'	MTK Helio P10 8 nhân 64-bit', N'	2 GB', N'	16 GB', N'	2300 mAh', 1, NULL, N'/assets/images/htc3.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (21, 1, 5, N'HTC One E9', N'Super LCD 3, 5.5", Full HD', N'	Android 5.0 (Lollipop)', N'	4 Ultra pixel', N'13 MP', 4041000, N'	MT6795 (Helio x10) 8 nhân 64-bit', N'	2 GB', N'	16 GB', N'	2800 mAh', 1, NULL, N'/assets/images/htc4.png', 1)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (22, 1, 6, N'Nokia 3', N'	IPS LCD, 5", HD', N'	Android 7.0', N'	8 MP', N'	8 MP', 3000000, N'	MT6737 4 nhân', N'	2 GB', N'	16 GB', N'	2650 mAh', 1, NULL, N'/assets/images/nokia1.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (23, 1, 6, N'Nokia 222', N'TFT, 2.4", 256.000 màu', N'	1000 số', N'Không', N'	2 MP', 950000, N'Không', N'2GB', N'1GB', N'	1100 mAh', 1, NULL, N'/assets/images/nokia2.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (24, 1, 6, N'Nokia K6', N'	IPS LCD, 5.5", Full HD', N'	Android 6.0 (Marshmallow)', N'	8 MP', N'	16 MP', 5990000, N'	Qualcomm Snapdragon 430 8 nhân 64 bit', N'	4 GB', N'	32 GB', N'	4000 mAh', 1, NULL, N'/assets/images/nokia3.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (25, 1, 6, N'Nokia VIBE K5', N'	IPS LCD, 5", Full HD', N'Android 5.1 (Lollipop)', N'	5 MP', N'	13 MP', 2691000, N'	Qualcomm Snapdragon 616 8 nhân 64-bit', N'	2 GB', N'	16 GB', N'	2750 mAh', 1, NULL, N'/assets/images/nokia4.png', 0)
INSERT [dbo].[SanPham] ([MaSanPham], [MaLoaiSanPham], [MaNhaSanXuat], [TenSanPham], [ManHinh], [HeDieuHanh], [CaneraTruoc], [CameraSau], [Gia], [CPU], [Ram], [BoNhoTrong], [DungLuongPin], [TinhTrang], [MoTa], [HinhAnh], [Home]) VALUES (26, 1, 2, N'sàaa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'<p>&agrave;aas</p>', N'/assets/images/2b71ceff-4c0e-4997-b7b5-deb4b60c7116.jpg.png', 3)
SET IDENTITY_INSERT [dbo].[SanPham] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 19/06/2017 12:29:46 SA ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 19/06/2017 12:29:46 SA ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 19/06/2017 12:29:46 SA ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 19/06/2017 12:29:46 SA ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 19/06/2017 12:29:46 SA ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 19/06/2017 12:29:46 SA ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[BinhLuan]  WITH CHECK ADD  CONSTRAINT [FK_BinhLuan_AspNetUsers] FOREIGN KEY([MaTaiKhoan])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[BinhLuan] CHECK CONSTRAINT [FK_BinhLuan_AspNetUsers]
GO
ALTER TABLE [dbo].[BinhLuan]  WITH CHECK ADD  CONSTRAINT [FK_BinhLuan_SanPham] FOREIGN KEY([MaSanPham])
REFERENCES [dbo].[SanPham] ([MaSanPham])
GO
ALTER TABLE [dbo].[BinhLuan] CHECK CONSTRAINT [FK_BinhLuan_SanPham]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_LoaiSanPham] FOREIGN KEY([MaLoaiSanPham])
REFERENCES [dbo].[LoaiSanPham] ([MaLoaiSanPham])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_LoaiSanPham]
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD  CONSTRAINT [FK_SanPham_NhaSanXuat] FOREIGN KEY([MaNhaSanXuat])
REFERENCES [dbo].[NhaSanXuat] ([MaNhaSanXuat])
GO
ALTER TABLE [dbo].[SanPham] CHECK CONSTRAINT [FK_SanPham_NhaSanXuat]
GO
USE [master]
GO
ALTER DATABASE [DoAnWebShopOnline] SET  READ_WRITE 
GO
