﻿using DoAnWeb_2.Areas.Admin.Models.BusAdmin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class GioHangAdminController : Controller
    {
        // GET: Admin/GioHangAdmin
        public ActionResult Index()
        {
            var sql = GioHangAdminBUS.DanhSachSanPham();
            return View(sql);
        }

        // GET: Admin/GioHangAdmin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/GioHangAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/GioHangAdmin/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/GioHangAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/GioHangAdmin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/GioHangAdmin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/GioHangAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
