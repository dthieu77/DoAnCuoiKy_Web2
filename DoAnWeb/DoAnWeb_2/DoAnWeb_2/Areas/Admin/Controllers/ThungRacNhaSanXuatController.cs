﻿using DoAnWeb_2.Areas.Admin.Models.BusAdmin;
using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class ThungRacNhaSanXuatController : Controller
    {
        [Authorize(Roles = "Admin")]
        // GET: Admin/ThungRacNhaSanXuat
        public ActionResult Index()
        {
            if (TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
            }
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
            }
            var sql = NhaSanXuatBusAdmin.DanhSachNhaSanXuatThungRac();
            return View(sql);
        }

        // GET: Admin/ThungRacNhaSanXuat/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/ThungRacNhaSanXuat/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/ThungRacNhaSanXuat/Create
        [HttpPost]
        public ActionResult Create(NhaSanXuat sp)
        {
            try
            {
                // TODO: Add insert logic here

                sp.TinhTrang = 0;

                NhaSanXuatBusAdmin.Them(sp);
                TempData["Success"] = "Thêm thành công !";
            }
            catch
            {
                TempData["Success"] = "Thêm không công !";
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/ThungRacNhaSanXuat/Edit/5
        public ActionResult Edit(int id)
        {
            var sql = NhaSanXuatBusAdmin.Update(id);
            return View(sql);
        }

        // POST: Admin/ThungRacNhaSanXuat/Edit/5
        [HttpPost]
        public ActionResult Edit( NhaSanXuat sp)
        {
            try
            {
                // TODO: Add update logic here
                sp.TinhTrang = 0;
                NhaSanXuatBusAdmin.UpdateDS(sp);
                TempData["Success"] = "Chỉnh sữa thành công !";
            }
            catch
            {
                TempData["Success"] = "Chỉnh sữa không công !";
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/ThungRacNhaSanXuat/Delete/5
      

        // POST: Admin/ThungRacNhaSanXuat/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                NhaSanXuatBusAdmin.KhoiPhuc(id);
                TempData["Success"] = "Xóa thành công !";
            }
            catch
            {
                TempData["Error"] = "Xóa không thành công !";
            }
            return RedirectToAction("Index");
        }
    }
}
