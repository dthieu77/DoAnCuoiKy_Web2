﻿using DoAnWeb_2.Areas.Admin.Models.BusAdmin;
using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class LoaiSanPhamAdminController : Controller
    {
        [Authorize(Roles = "Admin")]
        // GET: Admin/LoaiSanPhamAdmin
        public ActionResult Index()
        {
            if (TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
            }
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
            }
            var sql = LoaiSanPhamBusAdmin.DanhSachSanPham();
            return View(sql);
        }

        // GET: Admin/LoaiSanPhamAdmin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/LoaiSanPhamAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/LoaiSanPhamAdmin/Create
        [HttpPost]
        public ActionResult Create(LoaiSanPham sp)
        {
            try
            {
                // TODO: Add insert logic here

                sp.TinhTrang = 1;

                LoaiSanPhamBusAdmin.Them(sp);
                TempData["Success"] = "Thêm thành công !";
            }
            catch
            {
                TempData["Success"] = "Thêm không công !";
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/LoaiSanPhamAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            var sql = LoaiSanPhamBusAdmin.Update(id);
            return View(sql);
        }

        // POST: Admin/LoaiSanPhamAdmin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, LoaiSanPham sp)
        {
            try
            {
                // TODO: Add update logic here

                sp.TinhTrang = 1;
                LoaiSanPhamBusAdmin.UpdateDS(sp);
                TempData["Success"] = "Chỉnh sữa thành công !";
            }
            catch
            {
                TempData["Success"] = "Chỉnh sữa không công !";
            }
            return RedirectToAction("Index");
        }

    

        // POST: Admin/LoaiSanPhamAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {

            // TODO: Add delete logic here
            try
            {
                // TODO: Add delete logic here
                LoaiSanPhamBusAdmin.XoaTam(id);
                TempData["Success"] = "Xoa thanh cong !";
            }
            catch
            {
                TempData["Error"] = "Xoa khong thanh cong !";
            }

            return RedirectToAction("Index");
        }
    }
}
