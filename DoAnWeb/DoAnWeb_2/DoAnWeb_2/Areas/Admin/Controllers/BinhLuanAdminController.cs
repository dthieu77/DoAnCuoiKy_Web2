﻿using DoAnWeb_2.Areas.Admin.Models.BusAdmin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class BinhLuanAdminController : Controller
    {
        // GET: Admin/BinhLuanAdmin
        public ActionResult Index()
        {
            var sql = BinhLuanAdmin.DanhSachSanPham();
            return View(sql);
        }
        public ActionResult Delete(int id)
        {
            BinhLuanAdmin.XoaLienHe(id);
            return RedirectToAction("Index");
        }
    }
}