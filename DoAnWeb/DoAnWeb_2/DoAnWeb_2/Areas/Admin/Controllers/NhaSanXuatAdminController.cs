﻿using DoAnWeb_2.Areas.Admin.Models.BusAdmin;
using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class NhaSanXuatAdminController : Controller
    {
        [Authorize(Roles = "Admin")]
        // GET: Admin/NhaSanXuatAdmin
        public ActionResult Index()
        {
            if (TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
            }
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
            }
            var sql = NhaSanXuatBusAdmin.DanhSachSanPham();
            return View(sql);
        }

        // GET: Admin/NhaSanXuatAdmin/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/NhaSanXuatAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/NhaSanXuatAdmin/Create
        [HttpPost]
        public ActionResult Create(NhaSanXuat sp)
        {
            try
            {
                // TODO: Add insert logic here
                sp.TinhTrang = 1;
                
                NhaSanXuatBusAdmin.Them(sp);
                TempData["Success"] = "Thêm thành công !";
                
            }
            catch
            {
                TempData["Success"] = "Thêm không công !";
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/NhaSanXuatAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            var sql = NhaSanXuatBusAdmin.Update(id);
            return View(sql);
        }

        // POST: Admin/NhaSanXuatAdmin/Edit/5
        [HttpPost]
        public ActionResult Edit( NhaSanXuat sp)
        {
            try
            {
                // TODO: Add update logic here
                sp.TinhTrang = 1;
                NhaSanXuatBusAdmin.UpdateDS(sp);
                TempData["Success"] = "Chỉnh sữa thành công !";
            }
            catch
            {
                TempData["Success"] = "Chỉnh sữa không công !";
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/NhaSanXuatAdmin/Delete/5
     
        // POST: Admin/NhaSanXuatAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            
                // TODO: Add delete logic here
            try
            {
                // TODO: Add delete logic here
                NhaSanXuatBusAdmin.XoaTam(id);
                TempData["Success"] = "Xoa thanh cong !";
            }
            catch
            {
                TempData["Error"] = "Xoa khong thanh cong !";
            }

            return RedirectToAction("Index");

        }
    }
}
