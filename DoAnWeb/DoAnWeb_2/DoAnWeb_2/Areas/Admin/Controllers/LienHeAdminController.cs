﻿using DoAnWeb_2.Areas.Admin.Models.BusAdmin;
using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class LienHeAdminController : Controller
    {
        // GET: Admin/LienHeAdmin
        public ActionResult Index()
        {
            var sql = QuanLyLienHe.DanhSach();
            return View(sql);
        }

        // GET: Admin/LienHeAdmin/Details/5
        public ActionResult Details(int id)
        {

            return View();
        }

        // GET: Admin/LienHeAdmin/Create
        public ActionResult Create()
        {
            
            return View();
        }

        // POST: Admin/LienHeAdmin/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/LienHeAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            var sql = QuanLyLienHe.Update(id);
            return View(sql);
        }

        // POST: Admin/LienHeAdmin/Edit/5
        [HttpPost]
        public ActionResult Edit(LienHe sp)
        {
            try
            {
                // TODO: Add update logic here
                QuanLyLienHe.UpdateDS(sp);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/LienHeAdmin/Delete/5
        public ActionResult Delete(int id)
        {
            QuanLyLienHe.XoaLienHe(id);
            return RedirectToAction("Index");
        }

       
    }
}
