﻿using DoAnWeb_2.Areas.Admin.Models.BusAdmin;
using DoAnWeb2ShopConnection;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class ThungRacSanPhamController : Controller
    {
        // GET: Admin/ThungRacSanPham
        [Authorize(Roles = "Admin")]
        public ActionResult Index(int PagedList = 1, int size = 4)
        {

            if (TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
            }
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
            }
            var sql = SanPhamBusAdmin.DanhSachSanPhamThungRac().ToPagedList(PagedList, size);
            return View(sql);
        }

        // GET: Admin/ThungRacSanPham/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/ThungRacSanPham/Create
        public ActionResult Create()
        {

            ViewBag.MaLoaiSanPham = new SelectList(SanPhamBusAdmin.LoaiDS(), "MaLoaiSanPham", "TenLoaiSanPham");
            ViewBag.MaNhaSanXuat = new SelectList(SanPhamBusAdmin.LayNhaSanXuat(), "MaNhaSanXuat", "TenNhaSanXuat");
            return View();
        }

        // POST: Admin/ThungRacSanPham/Create
        [HttpPost, ValidateInput(false)]
        public ActionResult Create(SanPham sp)
        {
            try
            {
                if (HttpContext.Request.Files.Count > 0)
                {
                    var hpf = HttpContext.Request.Files[0];
                    if (hpf.ContentLength > 0)
                    {
                        string fileName = Guid.NewGuid().ToString();
                        string fullPathWithFileName = "/assets/images/" + fileName + ".jpg" + ".png";
                        hpf.SaveAs(Server.MapPath(fullPathWithFileName));
                        sp.HinhAnh = fullPathWithFileName;
                    }
                }
                // TODO: Add insert logic here
                sp.TinhTrang = 0;
                SanPhamBusAdmin.Them(sp);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/ThungRacSanPham/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.MaLoaiSanPham = new SelectList(SanPhamBusAdmin.LoaiDS(), "MaLoaiSanPham", "TenLoaiSanPham");
            ViewBag.MaNhaSanXuat = new SelectList(SanPhamBusAdmin.LayNhaSanXuat(), "MaNhaSanXuat", "TenNhaSanXuat");
            var sql = SanPhamBusAdmin.Updata(id);
            return View(sql);
        }

        // POST: Admin/ThungRacSanPham/Edit/5
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(SanPham sp)
        {
            try
            {
                if (HttpContext.Request.Files.Count > 0)
                {
                    var hpf = HttpContext.Request.Files[0];
                    if (hpf.ContentLength > 0)
                    {
                        string fileName = Guid.NewGuid().ToString();
                        string fullPathWithFileName = "/assets/images/" + fileName + ".jpg" + ".png";
                        hpf.SaveAs(Server.MapPath(fullPathWithFileName));
                        sp.HinhAnh = fullPathWithFileName;
                    }
                }
                // TODO: Add insert logic here
                sp.TinhTrang = 0;
                SanPhamBusAdmin.UptadaDS(sp);
                TempData["Success"] = "Chỉnh sữa thành công !";
            }
            catch
            {
                TempData["Success"] = "Chỉnh sữa không công !";
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/ThungRacSanPham/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/ThungRacSanPham/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult DeleteDS(int id)
        {
            try
            {
                // TODO: Add delete logic here
                SanPhamBusAdmin.DeleteThungRac(id);
                TempData["Success"] = "Xóa thành công !";
            }
            catch
            {
                TempData["Error"] = "Xóa không thành công  !";
            }

            return RedirectToAction("Index");
        }
    }
}
