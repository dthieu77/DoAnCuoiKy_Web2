﻿using DoAnWeb_2.Areas.Admin.Models.BusAdmin;
using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class ThungRacLoaiSanPhamController : Controller
    {
        [Authorize(Roles = "Admin")]
        // GET: Admin/ThungRacLoaiSanPham
        public ActionResult Index()
        {
            if (TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
            }
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
            }
            var sql = LoaiSanPhamBusAdmin.DanhSachThungRac();
            return View(sql);
        }

        // GET: Admin/ThungRacLoaiSanPham/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/ThungRacLoaiSanPham/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/ThungRacLoaiSanPham/Create
        [HttpPost]
        public ActionResult Create(LoaiSanPham sp)
        {
            try
            {
                // TODO: Add insert logic here

                sp.TinhTrang = 0;
                LoaiSanPhamBusAdmin.Them(sp);
                TempData["Success"] = "Thêm thành công !";
            }
            catch
            {
                TempData["Success"] = "Thêm không công !";
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/ThungRacLoaiSanPham/Edit/5
        public ActionResult Edit(int id)
        {
            var sql = LoaiSanPhamBusAdmin.Update(id);
            return View(sql);
        }

        // POST: Admin/ThungRacLoaiSanPham/Edit/5
        [HttpPost]
        public ActionResult Edit( LoaiSanPham sp)
        {
            try
            {
                // TODO: Add update logic here

                sp.TinhTrang = 0;
                LoaiSanPhamBusAdmin.UpdateDS(sp);
                TempData["Success"] = "Chỉnh sữa thành công !";
            }
            catch
            {
                TempData["Success"] = "Chỉnh sữa không công !";
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/ThungRacLoaiSanPham/Delete/5
      

        // POST: Admin/ThungRacLoaiSanPham/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {

            // TODO: Add delete logic here
            try
            {
                // TODO: Add delete logic here
                LoaiSanPhamBusAdmin.KhoiPhuc(id);
                TempData["Success"] = "Xóa thành công !";
            }
            catch
            {
                TempData["Error"] = "Xóa không thành công!";
            }

            return RedirectToAction("Index");
        }
    }
}
