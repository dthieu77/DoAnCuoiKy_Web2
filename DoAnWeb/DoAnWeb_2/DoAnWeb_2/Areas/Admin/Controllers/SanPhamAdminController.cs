﻿using DoAnWeb_2.Areas.Admin.Models.BusAdmin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using DoAnWeb2ShopConnection;
using DoAnWeb_2.Models.BUS;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class SanPhamAdminController : Controller
    {
        [Authorize(Roles = "Admin")]
        // GET: Admin/SanPhamAdmin
        public ActionResult Index(int PagedList = 1, int size = 4)
        {

            if (TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
            }
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
            }
            var sql = SanPhamBusAdmin.DanhSachSanPham().ToPagedList(PagedList, size);
            return View(sql);
        }

        // GET: Admin/SanPhamAdmin/Details/5
        public ActionResult Details(int id)
        {
            var sql = SanPhamBUS.ChiTietDS(id);
            return View(sql);
        }

        // GET: Admin/SanPhamAdmin/Create
        public ActionResult Create()
        {
            ViewBag.MaLoaiSanPham = new SelectList(SanPhamBusAdmin.LoaiDS(), "MaLoaiSanPham", "TenLoaiSanPham");
            ViewBag.MaNhaSanXuat = new SelectList(SanPhamBusAdmin.LayNhaSanXuat(), "MaNhaSanXuat", "TenNhaSanXuat");
            return View();
        }

        // POST: Admin/SanPhamAdmin/Create
        [HttpPost, ValidateInput(false)]
        public ActionResult Create(SanPham sp)
        {
            try
            {
                if (HttpContext.Request.Files.Count > 0)
                {
                    var hpf = HttpContext.Request.Files[0];
                    if (hpf.ContentLength > 0)
                    {
                        string fileName = Guid.NewGuid().ToString();
                        string fullPathWithFileName = "/assets/images/" + fileName + ".jpg" + ".png";
                        hpf.SaveAs(Server.MapPath(fullPathWithFileName));
                        sp.HinhAnh = fullPathWithFileName;
                    }
                }
                // TODO: Add insert logic here
                sp.TinhTrang = 1;
                SanPhamBusAdmin.Them(sp);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/SanPhamAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.MaLoaiSanPham = new SelectList(SanPhamBusAdmin.LoaiDS(), "MaLoaiSanPham", "TenLoaiSanPham");
            ViewBag.MaNhaSanXuat = new SelectList(SanPhamBusAdmin.LayNhaSanXuat(), "MaNhaSanXuat", "TenNhaSanXuat");
            var sql = SanPhamBusAdmin.Updata(id);
            return View(sql);
        }

        // POST: Admin/SanPhamAdmin/Edit/5
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(SanPham sp)
        {

            try
            {
                if (HttpContext.Request.Files.Count > 0)
                {
                    var hpf = HttpContext.Request.Files[0];
                    if (hpf.ContentLength > 0)
                    {
                        string fileName = Guid.NewGuid().ToString();
                        string fullPathWithFileName = "/assets/images/" + fileName + ".jpg" + ".png";
                        hpf.SaveAs(Server.MapPath(fullPathWithFileName));
                        sp.HinhAnh = fullPathWithFileName;
                    }
                }
                // TODO: Add insert logic here
                sp.TinhTrang = 1;
                SanPhamBusAdmin.UptadaDS(sp);
                TempData["Success"] = "Chỉnh sữa thành công !";
            }
            catch
            {
                TempData["Success"] = "Chỉnh sữa không công !";
            }
            return RedirectToAction("Index");
            //try
            //{
            //    // TODO: Add update logic here
            //    sp.TinhTrang = 1;
            //    SanPhamBusAdmin.UptadaDS(sp);
            //    TempData["Success"] = "Chỉnh sữa thành công !";

            //}
            //catch
            //{
            //    TempData["Success"] = "Chỉnh sữa không công !";
            //}
            //return RedirectToAction("Index");
        }

        // GET: Admin/SanPhamAdmin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/SanPhamAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult DeleteDS(int id)
        {
            try
            {
                // TODO: Add delete logic here
                SanPhamBusAdmin.DeleteDSSP(id);
                TempData["Success"] = "Xóa thành công !";
            }
            catch
            {
                TempData["Error"] = "Xóa không thành công  !";
            }

            return RedirectToAction("Index");
        }
    }
}
