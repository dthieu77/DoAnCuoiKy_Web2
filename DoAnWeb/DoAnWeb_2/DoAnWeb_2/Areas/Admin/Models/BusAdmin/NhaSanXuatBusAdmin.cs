﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Areas.Admin.Models.BusAdmin
{
    public class NhaSanXuatBusAdmin
    {
        public static IEnumerable<NhaSanXuat> DanhSachSanPham()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<NhaSanXuat>("select * from NhaSanXuat where TinhTrang = 1 ");
        }
        // them nha san xuat
        public static void Them(NhaSanXuat sx)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            sql.Insert(sx);
        }
        //xoa tam thoi
        public static void XoaTam(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                var lsp = db.Single<NhaSanXuat>("select * from NhaSanXuat where MaNhaSanXuat = @0", id);

                lsp.TinhTrang = 0;
                db.Update("NhaSanXuat", "MaNhaSanXuat", lsp);
            }
        }
        //chinh sua nha san xuat
        public static NhaSanXuat Update(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                return db.Single<NhaSanXuat>("select * from NhaSanXuat where MaNhaSanXuat =@0", id);
            }
        }
        public static void UpdateDS( NhaSanXuat sx)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            sql.Update("NhaSanXuat", "MaNhaSanXuat", sx);
        }

        //--------------------------------------------Thung ra-----------------------------------------//

        //load danh sach cua nha san xuat 
        public static IEnumerable<NhaSanXuat> DanhSachNhaSanXuatThungRac()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<NhaSanXuat>("select * from NhaSanXuat where TinhTrang = 0 ");
        }
        public static void KhoiPhuc(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                var lsp = db.Single<NhaSanXuat>("select * from NhaSanXuat where MaNhaSanXuat = @0", id);

                lsp.TinhTrang = 1;
                db.Update("NhaSanXuat", "MaNhaSanXuat", lsp);
            }
        }
    }
}