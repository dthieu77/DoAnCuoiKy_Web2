﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Areas.Admin.Models.BusAdmin
{
    public class BinhLuanAdmin
    {
        public static IEnumerable<BinhLuan> DanhSachSanPham()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<BinhLuan>("select * from BinhLuan");
        }
        public static int XoaLienHe(int id)
        {
            var db = new DoAnWeb2ShopConnectionDB();

            return db.Delete("BinhLuan", "MaBinhLuan", null, id);
        }
    }
}