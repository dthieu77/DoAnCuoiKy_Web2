﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Areas.Admin.Models.BusAdmin
{
    public class QuanLyLienHe
    {
        public static IEnumerable<LienHe> DanhSach()
        {
            DoAnWeb2ShopConnectionDB db =new DoAnWeb2ShopConnectionDB();
            return db.Query<LienHe>("select * from LienHe");
        }
        public static LienHe Update(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                return db.Single<LienHe>("select * from LienHe where ID =@0", id);
            }
        }
        public static void UpdateDS(LienHe sx)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            sql.Update("LienHe", "ID", sx);
        }

        public static int XoaLienHe(int id)
        {
            var db = new DoAnWeb2ShopConnectionDB();

            return db.Delete("LienHe", "ID", null, id);
        }

    }
}