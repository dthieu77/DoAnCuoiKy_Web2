﻿using DoAnWeb_2.Models.BUS;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Controllers
{
    public class GioHangController : Controller
    {
        // GET: GioHang
        public ActionResult Index()
        {
            ViewBag.TongTien = GioHangBUS.TongTien(User.Identity.GetUserId());
            return View(GioHangBUS.DanhSach(User.Identity.GetUserId()));
        }

        [HttpPost]
        public ActionResult Them(int masanpham, string tensanpham, int gia, int soluong)
        {
            //try
            //{

            GioHangBUS.Them(masanpham, User.Identity.GetUserId(), gia, soluong, tensanpham);
            return RedirectToAction("Index");
            //}
            // catch
            // {
            //  return View();
            //}
        }

        [HttpPost]
        public ActionResult CapNhat(int masanpham, string tensanpham, int gia, int soluong)
        {
            //try
            //{

            GioHangBUS.CapNhat(masanpham, User.Identity.GetUserId(), gia, soluong, tensanpham);
            return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return RedirectToAction("../SanPham/Index");
            //}
        }

        [HttpGet]

        public ActionResult Xoa(int masanpham)
        {
            try
            {

                GioHangBUS.Xoa(masanpham, User.Identity.GetUserId());
                return RedirectToAction("index");
            }
            catch
            {
                return RedirectToAction("../SanPham/index");
            }
        }
    }
}