﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using DoAnWeb_2.Models.BUS;
using Microsoft.AspNet.Identity;

namespace DoAnWeb_2.Controllers
{
    public class BinhLuanController : Controller
    {
        [Authorize]
        public ActionResult Create(int MaSanPham, String NoiDung)
        {

            BinhLuanBUS.Them(MaSanPham, User.Identity.GetUserId(), User.Identity.Name, NoiDung);
            return RedirectToAction("Details", "SanPham", new { Id = MaSanPham });
        }

        public ActionResult Index(int MaSanPham, int PagedList = 1, int size = 4)
        {
            ViewBag.MaSanPham = MaSanPham;
            return View(BinhLuanBUS.DanhSach(MaSanPham).ToPagedList(PagedList, size));
        }
    }
}