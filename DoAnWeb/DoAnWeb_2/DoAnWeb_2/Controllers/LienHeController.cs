﻿using DoAnWeb_2.Models.BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Controllers
{
    public class LienHeController : Controller
    {
        // GET: LienHe
        public ActionResult Index()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Them(string Name, string Email, string Phone, string DiaChi, string NoiDung)
        {
            try
            {

                LienHeBUS.Insert(Name, DiaChi, Phone, NoiDung, Email);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("../LienHe/Index");
            }
        }
    }
}