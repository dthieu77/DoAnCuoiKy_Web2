﻿using DoAnWeb_2.Models.BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var sql = SanPhamBUS.DanhSachTranHome();
            return View(sql);
        }
        public ActionResult Details(int id)
        {
            var sql = SanPhamBUS.ChiTietDS(id);
            return View(sql);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}