﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoAnWeb2ShopConnection;

namespace DoAnWeb_2.Models.BUS
{
    public class BinhLuanBUS
    {
        public static void Them(int MaSP, string MaTaiKhoan, string TenTaiKhoan, string NoiDung)
        {
            using (var sql = new DoAnWeb2ShopConnectionDB())
            {
                BinhLuan binhLuan = new BinhLuan();
                binhLuan.MaSanPham = MaSP;
                binhLuan.MaTaiKhoan = MaTaiKhoan;
                binhLuan.NoiDung = NoiDung;
                sql.Execute("INSERT INTO [dbo].[BinhLuan]([MaSanPham],[MaTaiKhoan],[TenTaiKhoan],[NoiDung]) VALUES (@0,@1,@2,@3)", MaSP, MaTaiKhoan, TenTaiKhoan, NoiDung);
            }
        }
        public static IEnumerable<BinhLuan> DanhSach(int MaSanPham)
        {
            using (var sql = new DoAnWeb2ShopConnectionDB())
            {
                return sql.Query<BinhLuan>(" select * from binhluan  where masanpham = @0 ORDER BY Ngay DESC", MaSanPham);
            }
        }
    }
}