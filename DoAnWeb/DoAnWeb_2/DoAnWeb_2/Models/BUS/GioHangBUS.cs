﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Models.BUS
{
    public class GioHangBUS
    {
        public static void Them(int masanpham, string mataikhoan, int gia, int soluong, string tensanpham)
        {
            using (var sql = new DoAnWeb2ShopConnectionDB())
            {
                var x = sql.Query<GioHang>("select * from GioHang Where MaTaiKhoan = '" + mataikhoan + "' and MaSanPham ='" + masanpham + "'").ToList();
                if (x.Count() > 0)
                {
                    //update so luong
                    int a = (int)x.ElementAt(0).SoLuong + soluong;
                    CapNhat(masanpham, mataikhoan, gia, a, tensanpham);
                }
                else
                {

                    GioHang giohang = new GioHang()
                    {
                        MaSanPham = masanpham,
                        MaTaiKhoan = mataikhoan,
                        Gia = gia,
                        SoLuong = soluong,
                        TenSP = tensanpham,
                        TongTien = gia * soluong
                    };
                    sql.Insert(giohang);
                }
            }
        }


        public static IEnumerable<GioHang> DanhSach(string mataikhoan)
        {
            using (var sql = new DoAnWeb2ShopConnectionDB())
            {
                return sql.Query<GioHang>("select * from GioHang where  MaTaiKhoan = '" + mataikhoan + "'");
            }
        }
        public static void CapNhat(int masanpham, string mataikhoan, int gia, int soluong, string tensanpham)
        {
            using (var sql = new DoAnWeb2ShopConnectionDB())
            {
                GioHang giohang = new GioHang()
                {
                    MaSanPham = masanpham,
                    MaTaiKhoan = mataikhoan,
                    Gia = gia,
                    SoLuong = soluong,
                    TenSP = tensanpham,
                    TongTien = gia * soluong

                };
                var tamp = sql.Query<GioHang>("select Id from GioHang Where MaTaiKhoan = '" + mataikhoan + "'and MaSanPham ='" + masanpham + "'").FirstOrDefault();
                sql.Update(giohang, tamp.Id);
            }

        }
        public static void Xoa(int masanpham, string mataikhoan)
        {
            using (var sql = new DoAnWeb2ShopConnectionDB())
            {
                var a = sql.Query<GioHang>("select * from GioHang Where MaTaiKhoan = '" + mataikhoan + "'and MaSanPham ='" + masanpham + "'").FirstOrDefault();
                sql.Delete(a);
            }
        }

        public static int TongTien(string mataikhoan)
        {
            using (var sql = new DoAnWeb2ShopConnectionDB())
            {
                return sql.Query<int>("select sum(TongTien) from GioHang where MaTaiKhoan = '" + mataikhoan + "'").FirstOrDefault();
            }
        }

    }
}