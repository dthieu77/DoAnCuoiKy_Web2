﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Models.BUS
{
    public class LienHeBUS
    {
        public static void Insert(string Name, string Email, string Phone, string DiaChi, string NoiDung)
        {
            using (var sql = new DoAnWeb2ShopConnectionDB())
            {
                LienHe lh = new LienHe();

                lh.Email = Email;
                lh.TenKhachHang = Name;
                lh.SDT = Phone;
                lh.DiaChi = DiaChi;
                lh.NoiDung = NoiDung;
                sql.Execute("INSERT INTO [dbo].[LienHe]([TenKhachHang],[DiaChi],[SDT],[NoiDung],[Email]) VALUES (@0,@1,@2,@3,@4)", Name, DiaChi, Phone, NoiDung, Email);
            }

        }
    }
}