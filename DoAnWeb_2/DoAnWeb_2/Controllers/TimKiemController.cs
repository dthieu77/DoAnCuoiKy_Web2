﻿using DoAnWeb_2.Models.BUS;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Controllers
{
    public class TimKiemController : Controller
    {
        [HttpGet]
        public ActionResult KetQuaTimKiem(string TimKiem, int page = 1, int pagesize = 3)
        {
            var db = TimKiemBUS.TimKiem(TimKiem).ToPagedList(page, pagesize);
            return View(db);
        }
    }
}