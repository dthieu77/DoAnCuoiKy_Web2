﻿using DoAnWeb_2.Models.BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Controllers
{
    public class NhaSanXuatController : Controller
    {
        // GET: NhaSanXuat
        public ActionResult Index()
        {
            var sql = SanPhamBUS.DanhSachSanPham();
            return View(sql);
        }
        public ActionResult AllProducts(int id)
        {
            var sql = NhaSanXuatBUS.TatCaSanPham(id);
            return View(sql);
        }
    }
}