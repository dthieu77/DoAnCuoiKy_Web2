﻿using DoAnWeb_2.Models.BUS;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Controllers
{
    public class SanPhamController : Controller
    {
        // GET: SanPham
        public ActionResult Index(int PagedList = 1, int size = 8)
        {
            var sql = SanPhamBUS.DanhSachSanPham().ToPagedList(PagedList, size);
            return View(sql);
        }

        public ActionResult Details(int id)
        {
            var sql = SanPhamBUS.ChiTietDS(id);
            return View(sql);
        }

    }
}