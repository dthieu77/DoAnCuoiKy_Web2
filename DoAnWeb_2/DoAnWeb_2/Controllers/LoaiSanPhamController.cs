﻿using DoAnWeb_2.Models.BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Controllers
{
    public class LoaiSanPhamController : Controller
    {
        // GET: LoaiSanPham
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AllProducts(int id)
        {
            var sql = LoaiSanPhamBUS.TatCaSanPham(id);
            return View(sql);
        }
    }
}