﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Models.BUS
{
    public class NhaSanXuatBUS
    {
        public static IEnumerable<NhaSanXuat> DanhSach()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<NhaSanXuat>("select * from NhaSanXuat where TinhTrang=1");
        }
      
        public static IEnumerable<SanPham> TatCaSanPham(int id)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<SanPham>("select * from SanPham where MaNhaSanXuat = @0", id).ToList();
        }
    }
}