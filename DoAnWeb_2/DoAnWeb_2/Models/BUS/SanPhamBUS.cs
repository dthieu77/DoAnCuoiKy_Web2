﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Models.BUS
{
    public class SanPhamBUS
    {
        public static IEnumerable<SanPham> DanhSachSanPham()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<SanPham>("select * from SanPham where TinhTrang = 1");
        }
        public static IEnumerable<SanPham> DanhSachTranHome()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<SanPham>("select * from SanPham where TinhTrang = 1");
        }
        public static IEnumerable<SanPham> DanhSachTranHome1()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<SanPham>("select * from SanPham where Home = 0");
        }
        public static SanPham ChiTietDS(int id)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.SingleOrDefault<SanPham>("select *from SanPham where MaSanPham=@0", id);
        }
    }
}