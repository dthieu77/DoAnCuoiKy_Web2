﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Models.BUS
{
    public class LoaiSanPhamBUS
    {
        public static IEnumerable<LoaiSanPham> DanhSach()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<LoaiSanPham>("select * from LoaiSanPham where TinhTrang=1");
        }

        public static IEnumerable<SanPham> TatCaSanPham(int id)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<SanPham>("select * from SanPham where MaLoaiSanPham = @0", id).ToList();
        }
    }
}