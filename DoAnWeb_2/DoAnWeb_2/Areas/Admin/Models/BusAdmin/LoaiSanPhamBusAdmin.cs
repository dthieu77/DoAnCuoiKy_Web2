﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Areas.Admin.Models.BusAdmin
{
    public class LoaiSanPhamBusAdmin
    {
        public static IEnumerable<LoaiSanPham> DanhSachSanPham()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<LoaiSanPham>("select * from LoaiSanPham where TinhTrang = 1 ");
        }
        // them nha san xuat
        public static void Them(LoaiSanPham sx)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            sql.Insert(sx);
        }
        //xoa tam thoi
        public static void XoaTam(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                var lsp = db.Single<LoaiSanPham>("select * from LoaiSanPham where MaLoaiSanPham = @0", id);

                lsp.TinhTrang = 0;
                db.Update("LoaiSanPham", "MaLoaiSanPham", lsp);
            }
        }
        //chinh sua nha san xuat
        public static LoaiSanPham Update(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                return db.Single<LoaiSanPham>("select * from LoaiSanPham where MaLoaiSanPham =@0", id);
            }
        }
        public static void UpdateDS(LoaiSanPham sx)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            sql.Update("LoaiSanPham", "MaLoaiSanPham", sx);
        }
        //------------------------------------------thung rac-------------------------------------//
        // load 
        public static IEnumerable<LoaiSanPham> DanhSachThungRac()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<LoaiSanPham>("select * from LoaiSanPham where TinhTrang = 0 ");
        }
        // koi phuc lai 
        public static void KhoiPhuc(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                var lsp = db.Single<LoaiSanPham>("select * from LoaiSanPham where MaLoaiSanPham = @0", id);

                lsp.TinhTrang = 1;
                db.Update("LoaiSanPham", "MaLoaiSanPham", lsp);
            }
        }

    }
}