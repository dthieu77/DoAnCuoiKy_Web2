﻿using DoAnWeb2ShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnWeb_2.Areas.Admin.Models.BusAdmin
{
    public class SanPhamBusAdmin
    {
        public static IEnumerable<SanPham> DanhSachSanPham()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<SanPham>("select * from SanPham where TinhTrang = 1 ");
        }

        public static void Them(SanPham sx)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            sql.Insert(sx);
        }
        //-----------------------------------------------------//
        public static IEnumerable<LoaiSanPham> LoaiDS()
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                return db.Query<LoaiSanPham>("select * from LoaiSanPham where TinhTrang=1");
            }
        }
        public static IEnumerable<NhaSanXuat> LayNhaSanXuat()
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                return db.Query<NhaSanXuat>("select * from NhaSanXuat where TinhTrang=1");
            }
        }
        //---------------------------------------------------------//
        public static SanPham Updata(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                return db.Single<SanPham>("select * from SanPham where MaSanPham =@0", id);
            }
        }
        public static void UptadaDS(SanPham sx)
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            sql.Update("SanPham", "MaSanPham", sx);
        }
        //-----xoa tam thoi-------------//
        public static void DeleteDSSP(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                var lsp = db.Single<SanPham>("select * from SanPham where MaSanPham = @0", id);

                lsp.TinhTrang = 0;
                db.Update("SanPham", "MaSanPham", lsp);
            }
        }

        //--------------thung rac-------------------//
        public static void Deletethungrac(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                var lsp = db.Single<SanPham>("select * from SanPham where MaSanPham = @0", id);

                lsp.TinhTrang = 1;
                db.Update("SanPham", "MaSanPham", lsp);
            }
        }
        public static IEnumerable<SanPham> DanhSachSanPhamThungRac()
        {
            var sql = new DoAnWeb2ShopConnectionDB();
            return sql.Query<SanPham>("select * from SanPham where TinhTrang = 0 ");
        }
        public static void DeleteThungRac(int id)
        {
            using (var db = new DoAnWeb2ShopConnectionDB())
            {
                var lsp = db.Single<SanPham>("select * from SanPham where MaSanPham = @0", id);

                lsp.TinhTrang = 1;
                db.Update("SanPham", "MaSanPham", lsp);
            }
        }

    }
}