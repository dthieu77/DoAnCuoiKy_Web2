﻿using DoAnWeb_2.Models.BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoAnWeb_2.Areas.Admin.Controllers
{
    public class LayoutAdminController : Controller
    {
        [Authorize(Roles = "Admin")]
        // GET: Admin/LayoutAdmin
        public ActionResult Index()
        {
            
            return View();
        }

        // GET: Admin/LayoutAdmin/Details/5
        public ActionResult Details(int id)
        {
            
            return View();
        }

        // GET: Admin/LayoutAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/LayoutAdmin/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/LayoutAdmin/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/LayoutAdmin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/LayoutAdmin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/LayoutAdmin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
